#!/usr/bin/env bash
echo $REDASH_BASE_PATH;

if [[ ! -e $REDASH_BASE_PATH ]]; then
    mkdir -p $REDASH_BASE_PATH
fi
if [[ ! -e $REDASH_BASE_PATH/postgres-data ]]; then
    mkdir $REDASH_BASE_PATH/postgres-data
    touch $REDASH_BASE_PATH/env
fi