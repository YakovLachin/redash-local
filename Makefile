REDASH_BASE_PATH=$(CURDIR)/volume

help:
	@echo 'Targets:'
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

create-network:
	@-docker network create dev-network

deps: ## Установка зависимостей
	@- sudo apt-get -yy install pwgen

volume: # Создаем
	@ REDASH_BASE_PATH=$(REDASH_BASE_PATH) bash ./volume.sh

volume/env: volume ## создание файла конфигруциионными переменныи окружения файла и диреткории с б.д.
	@ REDASH_BASE_PATH=$(REDASH_BASE_PATH) bash ./config.sh

up: ## Поднять redash
	@ docker-compose -f docker-compose.yml run  --rm server create_db
	@ docker-compose -f docker-compose.yml up -d

down: ## Остановить redash
	@- docker-compose down --rmi local

clean: ## Почистить всё, для сброса
	@ docker run --rm -v $(CURDIR):/data -w /data alpine rm -rf volume

.DEFAULT_GOAL=volume/env

.PHONY: up down clean deps create-network help